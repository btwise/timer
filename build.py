import PyInstaller.__main__
import os
import shutil

def clean_build_folders():
    """清理构建文件夹"""
    folders_to_clean = ['build', 'dist']
    for folder in folders_to_clean:
        if os.path.exists(folder):
            shutil.rmtree(folder)
            print(f"已清理 {folder} 文件夹")

def build_app():
    """构建应用程序"""
    # 清理旧的构建文件
    clean_build_folders()
    
    # 资源文件路径
    resources_dir = 'resources'
    icon_path = os.path.join(resources_dir, 'logo.ico')
    font_path = os.path.join(resources_dir, 'digital-7-mono-3.ttf')
    
    # 确保资源文件存在
    if not all(os.path.exists(path) for path in [icon_path, font_path]):
        print("错误：缺少必要的资源文件")
        return
    
    # PyInstaller 参数
    pyinstaller_args = [
        'src/main.py',  # 主程序入口
        '--name=包钢电气会议发言定时器',  # 应用程序名称
        '--icon=' + icon_path,  # 应用程序图标
        '--noconsole',  # 不显示控制台窗口
        '--noconfirm',  # 覆盖输出目录
        '--clean',  # 清理临时文件
        '--windowed',  # Windows下不显示控制台
        '--onefile',  # 打包成单个文件
        # 添加资源文件
        '--add-data', f'{resources_dir};resources',
        # 优化选项
        '--noupx',  # 不使用UPX压缩
        '--strip',  # 减小二进制文件大小
        # 排除不需要的模块
        '--exclude-module=matplotlib',
        '--exclude-module=numpy',
        '--exclude-module=PIL',
        '--exclude-module=pandas',
        '--exclude-module=scipy',
        # 只包含必要的模块
        '--hidden-import=win32com.client',
        '--hidden-import=pythoncom',
    ]
    
    print("开始构建应用程序...")
    PyInstaller.__main__.run(pyinstaller_args)
    print("构建完成！")
    
    # 检查构建结果
    exe_path = os.path.join('dist', '包钢电气会议发言定时器.exe')
    if os.path.exists(exe_path):
        print(f"应用程序已生成在: {os.path.abspath(exe_path)}")
        print(f"文件大小: {os.path.getsize(exe_path) / 1024 / 1024:.2f} MB")
    else:
        print("构建失败！")

if __name__ == '__main__':
    build_app() 