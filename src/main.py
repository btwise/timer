import sys
import os

# 在创建QApplication之前设置环境变量来禁用图像警告
os.environ["QT_LOGGING_RULES"] = "qt.gui.imageio.png=false"

from PyQt6.QtWidgets import QApplication
from PyQt6.QtGui import QPalette, QColor
from main_window import MainWindow

def main():
    app = QApplication(sys.argv)
    
    # 设置应用程序样式
    app.setStyle('Fusion')
    
    # 创建自定义调色板
    palette = QPalette()
    palette.setColor(QPalette.ColorRole.Window, QColor(245, 246, 250))
    palette.setColor(QPalette.ColorRole.WindowText, QColor(44, 62, 80))
    palette.setColor(QPalette.ColorRole.Base, QColor(255, 255, 255))
    palette.setColor(QPalette.ColorRole.AlternateBase, QColor(248, 249, 253))
    palette.setColor(QPalette.ColorRole.Text, QColor(44, 62, 80))
    palette.setColor(QPalette.ColorRole.Button, QColor(52, 152, 219))
    palette.setColor(QPalette.ColorRole.ButtonText, QColor(255, 255, 255))
    palette.setColor(QPalette.ColorRole.Highlight, QColor(41, 128, 185))
    palette.setColor(QPalette.ColorRole.HighlightedText, QColor(255, 255, 255))
    
    app.setPalette(palette)
    
    # 设置全局样式表
    app.setStyleSheet("""
        QWidget {
            font-family: 'Microsoft YaHei UI', 'Segoe UI', sans-serif;
            font-size: 14px;
        }
        QMainWindow {
            background-color: #f5f6fa;
        }
        QPushButton {
            border-radius: 8px;
            padding: 8px 24px;
            font-weight: bold;
        }
        QTimeEdit {
            border-radius: 8px;
            padding: 8px 12px;
            background-color: white;
            selection-background-color: #3498db;
            selection-color: white;
        }
    """)
    
    window = MainWindow()
    window.show()
    
    sys.exit(app.exec())

if __name__ == '__main__':
    main() 