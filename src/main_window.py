from PyQt6.QtWidgets import (QMainWindow, QWidget, QPushButton, 
                             QVBoxLayout, QHBoxLayout, QTimeEdit, QSystemTrayIcon,
                             QMenu, QStyle, QFrame, QMessageBox, QDialog, QLabel,
                             QSpinBox, QCheckBox, QComboBox)
from PyQt6.QtCore import Qt, QTime, QTimer, QPropertyAnimation, QPoint, QRect
from PyQt6.QtGui import QAction, QIcon, QPalette, QColor, QPainter, QLinearGradient
from countdown_styles import CircularCountdown, BubbleCountdown, ClockCountdown
from date_clock import DateClockWidget
import sys
import os
import json
import logging
from pathlib import Path
from PyQt6.QtWidgets import QApplication

# 忽略 libpng 警告
logging.getLogger('Qt6.QtGui.QImageReader').setLevel(logging.ERROR)

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        # 保持应用程序运行的引用
        self.app = QApplication.instance()
        # 加载设置
        self.load_settings()
        self.initUI()
        
        # 初始化日期时钟
        self.date_clock = None
        if self.show_date_clock:
            self.show_date_clock_widget()
        
    def load_settings(self):
        # 获取应用数据目录
        app_data_dir = os.path.join(os.getenv('APPDATA'), '包钢电气会议发言定时器')
        # 确保目录存在
        os.makedirs(app_data_dir, exist_ok=True)
        # 设置文件路径
        self.settings_file = os.path.join(app_data_dir, 'settings.json')
        
        # 默认设置
        self.countdown_seconds = 60
        self.enable_voice = True
        self.countdown_style = "圆环进度条"
        self.show_date_clock = False  # 添加日期时钟设置
        
        # 如果设置文件存在，则加载设置
        if os.path.exists(self.settings_file):
            try:
                with open(self.settings_file, 'r', encoding='utf-8') as f:
                    settings = json.load(f)
                    self.countdown_seconds = settings.get('countdown_seconds', 60)
                    self.enable_voice = settings.get('enable_voice', True)
                    self.countdown_style = settings.get('countdown_style', "圆环进度条")
                    self.show_date_clock = settings.get('show_date_clock', False)  # 加载日期时钟设置
            except Exception as e:
                print(f"加载设置文件出错: {e}")
                
    def save_settings(self):
        # 保存设置到文件
        try:
            settings = {
                'countdown_seconds': self.countdown_seconds,
                'enable_voice': self.enable_voice,
                'countdown_style': self.countdown_style,
                'show_date_clock': self.show_date_clock  # 保存日期时钟设置
            }
            with open(self.settings_file, 'w', encoding='utf-8') as f:
                json.dump(settings, f, ensure_ascii=False, indent=4)
        except Exception as e:
            print(f"保存设置文件出错: {e}")
        
    def initUI(self):
        # 设置窗口基本属性
        self.setWindowTitle('包钢电气会议发言定时器')
        self.setWindowFlags(Qt.WindowType.WindowStaysOnTopHint)
        self.setFixedSize(500, 80)  # 调整窗口大小
        
        # 设置窗口图标
        self.setup_system_tray()
        
        # 创建中央窗口部件
        central_widget = QWidget()
        self.setCentralWidget(central_widget)
        
        # 创建主布局
        main_layout = QHBoxLayout(central_widget)
        main_layout.setContentsMargins(20, 15, 20, 15)
        main_layout.setSpacing(20)
        
        # 创建时间编辑器
        self.time_edit = QTimeEdit()
        self.time_edit.setDisplayFormat("HH:mm:ss")
        self.time_edit.setTime(QTime(0, 0, 0))
        self.time_edit.setFixedWidth(160)  # 增加宽度以适应所有字段
        self.time_edit.setFixedHeight(36)  # 固定高度
        self.time_edit.setStyleSheet("""
            QTimeEdit {
                background-color: rgba(255, 255, 255, 0.12);
                border: none;
                border-radius: 5px;
                padding: 0px 32px 0px 10px;  /* 调整内边距 */
                color: #ffffff;
                font-size: 22px;  /* 调整字体大小 */
                font-weight: bold;
            }
            QTimeEdit:hover {
                background-color: rgba(255, 255, 255, 0.15);
            }
            QTimeEdit:focus {
                background-color: rgba(255, 255, 255, 0.18);
            }
            QTimeEdit::section {
                color: #ffffff;
                background-color: transparent;
            }
            QTimeEdit::up-button {
                width: 18px;
                height: 16px;
                border: none;
                background-color: rgba(255, 255, 255, 0.15);
                border-top-right-radius: 3px;
                subcontrol-position: top right;
                subcontrol-origin: padding;
                margin-right: 4px;
                margin-top: 2px;
            }
            QTimeEdit::down-button {
                width: 18px;
                height: 16px;
                border: none;
                background-color: rgba(255, 255, 255, 0.15);
                border-bottom-right-radius: 3px;
                subcontrol-position: bottom right;
                subcontrol-origin: padding;
                margin-right: 4px;
                margin-bottom: 2px;
            }
            QTimeEdit::up-button:hover, QTimeEdit::down-button:hover {
                background-color: rgba(255, 255, 255, 0.25);
            }
            QTimeEdit::up-button:pressed, QTimeEdit::down-button:pressed {
                background-color: rgba(255, 255, 255, 0.3);
            }
            QTimeEdit::up-arrow {
                border-style: solid;
                border-width: 0 3px 3px 3px;
                border-color: transparent transparent white transparent;
                width: 0;
                height: 0;
            }
            QTimeEdit::down-arrow {
                border-style: solid;
                border-width: 3px 3px 0 3px;
                border-color: white transparent transparent transparent;
                width: 0;
                height: 0;
            }
        """)
        
        # 创建按钮布局
        button_layout = QHBoxLayout()
        button_layout.setSpacing(12)
        
        # 创建按钮
        self.start_button = QPushButton('开始')
        self.stop_button = QPushButton('停止')
        self.settings_button = QPushButton('设置')
        
        # 设置所有按钮的固定大小
        for button in [self.start_button, self.stop_button, self.settings_button]:
            button.setFixedSize(80, 36)
        
        # 设置按钮样式
        button_style = """
            QPushButton {
                padding: 0;
                border-radius: 5px;
                font-weight: bold;
                font-size: 15px;
                border: none;
                background: qlineargradient(x1:0, y1:0, x2:1, y2:0, 
                    stop:0 %s, stop:1 %s);
                color: white;
            }
            QPushButton:hover {
                background: qlineargradient(x1:0, y1:0, x2:1, y2:0, 
                    stop:0 %s, stop:1 %s);
            }
            QPushButton:pressed {
                background: qlineargradient(x1:0, y1:0, x2:1, y2:0, 
                    stop:0 %s, stop:1 %s);
                margin-top: 1px;
                margin-bottom: -1px;
            }
            QPushButton:disabled {
                background: rgba(52, 73, 94, 0.7);
                color: rgba(255, 255, 255, 0.4);
            }
        """
        
        self.start_button.setStyleSheet(button_style % 
            ('#2ecc71', '#27ae60',  # normal
             '#27ae60', '#219a52',  # hover
             '#219a52', '#1e8449')) # pressed
             
        self.stop_button.setStyleSheet(button_style % 
            ('#e74c3c', '#c0392b',  # normal
             '#c0392b', '#a93226',  # hover
             '#a93226', '#962d22')) # pressed
             
        self.settings_button.setStyleSheet(button_style % 
            ('#3498db', '#2980b9',  # normal
             '#2980b9', '#2471a3',  # hover
             '#2471a3', '#1f618d')) # pressed
        
        # 初始状态下停止按钮禁用
        self.stop_button.setEnabled(False)
        
        # 添加按钮到布局
        button_layout.addWidget(self.start_button)
        button_layout.addWidget(self.stop_button)
        button_layout.addWidget(self.settings_button)
        
        # 添加所有布局到主布局
        main_layout.addWidget(self.time_edit)
        main_layout.addLayout(button_layout)
        
        # 设置窗口背景
        self.setStyleSheet("""
            QMainWindow {
                background: qlineargradient(x1:0, y1:0, x2:1, y2:1,
                    stop:0 #2c3e50,
                    stop:0.5 #34495e,
                    stop:1 #2c3e50
                );
            }
            QWidget {
                font-family: 'Microsoft YaHei UI', 'Segoe UI', sans-serif;
            }
        """)
        
        # 连接信号
        self.start_button.clicked.connect(self.start_timer)
        self.stop_button.clicked.connect(self.stop_timer)
        self.settings_button.clicked.connect(self.show_settings)
        
        # 初始化计时器
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_timer)
        
    def setup_system_tray(self):
        self.tray_icon = QSystemTrayIcon(self)
        
        # 获取资源文件路径
        if getattr(sys, 'frozen', False):
            # 如果是打包后的exe
            base_path = sys._MEIPASS
        else:
            # 如果是开发环境
            base_path = os.path.dirname(os.path.dirname(__file__))
            
        icon_path = os.path.join(base_path, 'resources', 'logo.png')
        
        try:
            self.app_icon = QIcon(icon_path)
            if not self.app_icon.isNull():
                self.setWindowIcon(self.app_icon)
                self.tray_icon.setIcon(self.app_icon)
            else:
                print(f"图标加载失败: {icon_path}")
                self.app_icon = self.style().standardIcon(QStyle.StandardPixmap.SP_TitleBarMenuButton)
                self.tray_icon.setIcon(self.app_icon)
        except Exception as e:
            print(f"加载图标出错: {e}")
            self.app_icon = self.style().standardIcon(QStyle.StandardPixmap.SP_TitleBarMenuButton)
            self.tray_icon.setIcon(self.app_icon)
        
        # 创建托盘菜单
        tray_menu = QMenu()
        show_action = QAction("显示", self)
        quit_action = QAction("退出", self)
        
        show_action.triggered.connect(self.show)
        quit_action.triggered.connect(self.quit_application)
        
        tray_menu.addAction(show_action)
        tray_menu.addAction(quit_action)
        
        self.tray_icon.setContextMenu(tray_menu)
        self.tray_icon.activated.connect(self._handle_tray_activation)
        self.tray_icon.show()
        
    def _handle_tray_activation(self, reason):
        # 响应托盘图标的双击事件
        if reason == QSystemTrayIcon.ActivationReason.DoubleClick:
            self.show()  # 显示主窗口
            self.raise_()  # 确保窗口在最前面
            self.activateWindow()  # 激活窗口
        
    def quit_application(self):
        """正确退出应用程序"""
        self.hide_date_clock_widget()  # 关闭日期时钟
        self.tray_icon.hide()
        self.app.quit()
        
    def start_timer(self):
        time = self.time_edit.time()
        self.remaining_time = time.hour() * 3600 + time.minute() * 60 + time.second()
        if self.remaining_time <= 0:
            return  # 如果时间为0，不启动计时器
            
        # 计算动画显示时间点
        if self.remaining_time <= self.countdown_seconds:
            # 如果总时间小于设置的提前提醒时间，则在剩余时间的1/5处显示动画
            self.alert_time = max(1, int(self.remaining_time * 0.2))
        else:
            # 否则使用设置的提前提醒时间
            self.alert_time = self.countdown_seconds
            
        self.timer.start(1000)  # 每秒更新一次
        self.start_button.setEnabled(False)
        self.stop_button.setEnabled(True)
        self.time_edit.setEnabled(False)
        
        # 最小化窗口到托盘
        self.hide()
        # 显示托盘提示
        self.tray_icon.showMessage(
            "计时器已启动",
            f"将在{time.toString('HH:mm:ss')}后提醒您",
            QSystemTrayIcon.MessageIcon.Information,
            2000
        )
        
    def stop_timer(self):
        self.timer.stop()
        self.start_button.setEnabled(True)
        self.stop_button.setEnabled(False)
        self.time_edit.setEnabled(True)
        
    def update_timer(self):
        self.remaining_time -= 1
        if self.remaining_time <= 0:
            self.timer.stop()
            self.stop_timer()
            self.time_edit.setTime(QTime(0, 0, 0))  # 计时结束后重置为0
            # 显示主窗口
            self.show()
            self.raise_()  # 确保窗口在最前面
            self.activateWindow()  # 激活窗口
            # 显示托盘提示
            self.tray_icon.showMessage(
                "计时结束",
                "计时已结束，请设置新的时间",
                QSystemTrayIcon.MessageIcon.Information,
                2000
            )
        else:
            hours = self.remaining_time // 3600
            minutes = (self.remaining_time % 3600) // 60
            seconds = self.remaining_time % 60
            self.time_edit.setTime(QTime(hours, minutes, seconds))
            self.tray_icon.setToolTip(f"剩余时间: {hours:02d}:{minutes:02d}:{seconds:02d}")
            # 检查是否需要显示倒计时提醒
            if self.remaining_time == self.alert_time:
                self.show_countdown_alert()
        
    def show_settings(self):
        settings_dialog = SettingsDialog(self)
        if settings_dialog.exec() == QDialog.DialogCode.Accepted:
            # 保存设置
            self.countdown_seconds = settings_dialog.countdown_spinbox.value()
            self.enable_voice = settings_dialog.voice_checkbox.isChecked()
            self.countdown_style = settings_dialog.style_combo.currentText()
            
            # 处理日期时钟设置
            new_show_date_clock = settings_dialog.date_clock_checkbox.isChecked()
            if new_show_date_clock != self.show_date_clock:
                self.show_date_clock = new_show_date_clock
                if self.show_date_clock:
                    self.show_date_clock_widget()
                else:
                    self.hide_date_clock_widget()
            
            # 保存到文件
            self.save_settings()
            
    def show_date_clock_widget(self):
        if not self.date_clock:
            self.date_clock = DateClockWidget()
            # 设置初始位置在屏幕顶部中央
            screen = self.screen()
            self.date_clock.setGeometry(
                screen.geometry().center().x() - 300,  # 默认宽度600
                0,  # 顶部
                600,  # 默认宽度
                150   # 默认高度
            )
        self.date_clock.show()
        
    def hide_date_clock_widget(self):
        if self.date_clock:
            self.date_clock.close()
            self.date_clock = None
            
    def show_countdown_alert(self):
        # 根据设置选择不同的倒计时样式
        if self.countdown_style == "圆环进度条":
            self.countdown_alert = CircularCountdown(enable_voice=self.enable_voice)
            self.countdown_alert.setFixedSize(300, 300)
            # 在屏幕中央显示
            screen = self.screen()
            self.countdown_alert.setGeometry(
                screen.geometry().center().x() - 150,
                screen.geometry().center().y() - 150,
                300, 300
            )
        elif self.countdown_style == "气泡提示框":
            self.countdown_alert = BubbleCountdown(enable_voice=self.enable_voice)
            # 在右下角显示，但要留出足够空间
            screen = self.screen()
            # 计算气泡位置，确保完全显示（包括小三角）
            bubble_width = 300
            bubble_height = 160
            margin = 30  # 距离屏幕边缘的边距
            
            self.countdown_alert.setGeometry(
                screen.geometry().right() - bubble_width - margin,
                screen.geometry().bottom() - bubble_height - margin - 20,  # 额外留出小三角的空间
                bubble_width,
                bubble_height
            )
        else:  # 数字时钟
            self.countdown_alert = ClockCountdown(enable_voice=self.enable_voice)
            # 在屏幕中央显示
            screen = self.screen()
            self.countdown_alert.setGeometry(
                screen.geometry().center().x() - 500,  # 调整为新的宽度的一半
                screen.geometry().center().y() - 100,  # 调整为新的高度的一半
                1000, 200  # 使用新的尺寸
            )
            
        # 设置初始时间显示
        if self.countdown_style == "数字时钟":
            self.countdown_alert.setText("00:00:{:02d}".format(self.alert_time))
        elif self.countdown_style == "圆环进度条":
            self.countdown_alert.setText(str(self.alert_time))
        else:
            self.countdown_alert.time_label.setText(str(self.alert_time))
        
        # 创建进度动画
        self.progress_animation = QPropertyAnimation(self.countdown_alert, b"progress")
        self.progress_animation.setDuration(self.alert_time * 1000)
        self.progress_animation.setStartValue(1.0)
        self.progress_animation.setEndValue(0.0)
        
        # 开始倒计时
        self.countdown_alert.speak_text("请注意，时间即将结束")
        self.countdown_alert.show()
        self.progress_animation.start()
        
        # 创建定时器更新显示的时间
        self.countdown_timer = QTimer(self)
        self.countdown_time = self.alert_time
        
        def update_time():
            self.countdown_time -= 1
            if self.countdown_style == "数字时钟":
                self.countdown_alert.setText("00:00:{:02d}".format(self.countdown_time))
            elif self.countdown_style == "圆环进度条":
                self.countdown_alert.setText(str(self.countdown_time))
            else:
                self.countdown_alert.time_label.setText(str(self.countdown_time))
            if self.countdown_time <= 0:
                self.countdown_timer.stop()
                self.countdown_alert.close()
                
        self.countdown_timer.timeout.connect(update_time)
        self.countdown_timer.start(1000)

    def closeEvent(self, event):
        # 保存设置
        self.save_settings()
        
        msg = QMessageBox(self)
        msg.setWindowTitle("退出确认")
        msg.setText("是否最小化到系统托盘？")
        msg.setIcon(QMessageBox.Icon.Question)
        
        # 使用标准按钮
        msg.setStandardButtons(
            QMessageBox.StandardButton.Yes | 
            QMessageBox.StandardButton.No | 
            QMessageBox.StandardButton.Cancel
        )
        
        # 设置按钮文字
        yes_btn = msg.button(QMessageBox.StandardButton.Yes)
        no_btn = msg.button(QMessageBox.StandardButton.No)
        cancel_btn = msg.button(QMessageBox.StandardButton.Cancel)
        
        yes_btn.setText("最小化到托盘")
        no_btn.setText("退出程序")
        cancel_btn.setText("取消")
        
        # 设置默认按钮
        msg.setDefaultButton(QMessageBox.StandardButton.Yes)
        
        # 设置对话框样式，确保文字清晰可见
        msg.setStyleSheet("""
            QMessageBox {
                background-color: #f0f0f0;
            }
            QMessageBox QLabel {
                color: #000000;
            }
            QPushButton {
                color: #000000;
                background-color: #e1e1e1;
                border: 1px solid #c0c0c0;
                padding: 5px 15px;
                border-radius: 2px;
                font-weight: normal;
            }
            QPushButton:hover {
                background-color: #e9e9e9;
            }
            QPushButton:pressed {
                background-color: #d0d0d0;
            }
        """)
        
        # 执行对话框
        ret = msg.exec()
        
        if ret == QMessageBox.StandardButton.Yes:
            event.ignore()
            self.hide()
        elif ret == QMessageBox.StandardButton.No:
            self.quit_application()
        else:  # Cancel
            event.ignore()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.RenderHint.Antialiasing)
        
        # 绘制渐变背景
        gradient = QLinearGradient(0, 0, self.width(), self.height())
        gradient.setColorAt(0, QColor(41, 128, 185, 250))  # 蓝色
        gradient.setColorAt(0.5, QColor(52, 152, 219, 250))  # 浅蓝色
        gradient.setColorAt(1, QColor(41, 128, 185, 250))  # 蓝色
        painter.fillRect(self.rect(), gradient)
        
        # 绘制装饰图案
        painter.setPen(QColor(255, 255, 255, 15))
        
        # 绘制网格线
        for i in range(0, self.width(), 30):
            painter.drawLine(i, 0, i, self.height())
        for i in range(0, self.height(), 30):
            painter.drawLine(0, i, self.width(), i)
            
        # 绘制对角线
        for i in range(-self.height(), self.width(), 50):
            painter.drawLine(i, 0, i + self.height(), self.height())

class SettingsDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        # 获取主窗口的当前设置
        self.main_window = parent
        self.initUI()
        # 设置控件的初始值为上次保存的值
        self.countdown_spinbox.setValue(self.main_window.countdown_seconds)
        self.voice_checkbox.setChecked(self.main_window.enable_voice)
        self.style_combo.setCurrentText(self.main_window.countdown_style)
        self.date_clock_checkbox.setChecked(getattr(self.main_window, 'show_date_clock', False))
        
    def initUI(self):
        self.setWindowTitle("设置")
        self.setFixedSize(300, 300)  # 增加高度以适应新控件
        
        # 创建布局
        layout = QVBoxLayout()
        layout.setSpacing(20)
        layout.setContentsMargins(20, 20, 20, 20)
        
        # 倒计时设置
        countdown_layout = QHBoxLayout()
        countdown_label = QLabel("提前提醒时间：")
        countdown_label.setFixedWidth(100)
        countdown_label.setAlignment(Qt.AlignmentFlag.AlignRight | Qt.AlignmentFlag.AlignVCenter)
        self.countdown_spinbox = QSpinBox()
        self.countdown_spinbox.setFixedWidth(150)
        self.countdown_spinbox.setRange(1, 60)
        self.countdown_spinbox.setSuffix(" 秒")
        countdown_layout.addWidget(countdown_label)
        countdown_layout.addWidget(self.countdown_spinbox)
        countdown_layout.addStretch()
        
        # 倒计时风格设置
        style_layout = QHBoxLayout()
        style_label = QLabel("倒计时风格：")
        style_label.setFixedWidth(100)
        style_label.setAlignment(Qt.AlignmentFlag.AlignRight | Qt.AlignmentFlag.AlignVCenter)
        self.style_combo = QComboBox()
        self.style_combo.setFixedWidth(150)
        self.style_combo.addItems([
            "圆环进度条",
            "气泡提示框",
            "数字时钟"
        ])
        style_layout.addWidget(style_label)
        style_layout.addWidget(self.style_combo)
        style_layout.addStretch()
        
        # 语音提醒设置
        self.voice_checkbox = QCheckBox("启用语音提醒")
        
        # 日期时钟设置
        self.date_clock_checkbox = QCheckBox("启用日期时钟")
        self.date_clock_checkbox.setToolTip("在屏幕顶部显示数字时钟和日期")
        
        # 添加按钮
        button_layout = QHBoxLayout()
        ok_button = QPushButton("确定")
        cancel_button = QPushButton("取消")
        ok_button.clicked.connect(self.accept)
        cancel_button.clicked.connect(self.reject)
        button_layout.addWidget(ok_button)
        button_layout.addWidget(cancel_button)
        
        # 设置样式
        self.setStyleSheet("""
            QDialog {
                background-color: #f0f0f0;
            }
            QLabel {
                color: #2c3e50;
                font-size: 14px;
            }
            QSpinBox, QComboBox {
                padding: 5px;
                border: 1px solid #bdc3c7;
                border-radius: 4px;
                background: white;
                min-width: 120px;
                color: #2c3e50;
                font-size: 14px;
            }
            QComboBox::drop-down {
                border: none;
                border-left: 1px solid #bdc3c7;
                width: 25px;
                background: #f8f9fa;
            }
            QComboBox::down-arrow {
                width: 0;
                height: 0;
                border: 6px solid transparent;
                border-top: 6px solid #666;
                margin-top: 5px;
            }
            QComboBox:hover {
                border: 1px solid #3498db;
                background: #f8f9fa;
            }
            QComboBox::drop-down:hover {
                background: #e8f0fe;
                border-left: 1px solid #3498db;
            }
            QComboBox:on {  /* 当下拉框打开时 */
                background: #e8f0fe;
                border: 1px solid #3498db;
            }
            QComboBox:on::drop-down {
                background: #e8f0fe;
                border-left: 1px solid #3498db;
            }
            QComboBox QAbstractItemView {  /* 下拉列表样式 */
                background-color: white;
                border: 1px solid #bdc3c7;
                selection-background-color: #3498db;
                selection-color: white;
                outline: none;
            }
            QComboBox QAbstractItemView::item {
                padding: 8px 10px;
                min-height: 25px;
                color: #2c3e50;
            }
            QComboBox QAbstractItemView::item:hover {
                background-color: #3498db;
                color: white;
            }
            QComboBox QAbstractItemView::item:selected {
                background-color: #2980b9;
                color: white;
            }
            QCheckBox {
                color: #2c3e50;
                font-size: 14px;
                padding: 5px;
            }
            QCheckBox:hover {
                background-color: rgba(52, 152, 219, 0.1);
                border-radius: 4px;
            }
            QPushButton {
                padding: 8px 16px;
                border-radius: 4px;
                border: none;
                color: white;
                font-weight: bold;
                min-width: 80px;
            }
            QPushButton[text="确定"] {
                background-color: #3498db;
            }
            QPushButton[text="确定"]:hover {
                background-color: #2980b9;
            }
            QPushButton[text="取消"] {
                background-color: #e74c3c;
            }
            QPushButton[text="取消"]:hover {
                background-color: #c0392b;
            }
        """)
        
        # 添加所有控件到主布局
        layout.addLayout(countdown_layout)
        layout.addLayout(style_layout)
        layout.addWidget(self.voice_checkbox)
        layout.addWidget(self.date_clock_checkbox)  # 添加日期时钟选项
        layout.addStretch()
        layout.addLayout(button_layout)
        
        self.setLayout(layout) 